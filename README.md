### Golang Aws Lambda Slack Channel Created

SlackのChannel作成ををAWSのLambdaとGatewayで検知してgeneralチャンネルに通知する

## 環境

* Windows 10
* Golang 1.12

## Buildコマンド

```bash
$ GOOS=linux go build -o SlackChannelCreated main.go
$ build-lambda-zip.exe -o SlackChannelCreated.zip SlackChannelCreated
```

### 備考

`webhook url`の部分はslackのnotifyのURLに変更する
