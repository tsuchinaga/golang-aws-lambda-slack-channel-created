package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"net/http"
)

type channelCreatedRequest struct {
	Event struct {
		Type    string `json:"type"`
		Channel struct {
			Id      string `json:"id"`
			Name    string `json:"name"`
			Created int    `json:"created"`
			Creator string `json:"creator"`
		} `json:"channel"`
	} `json:"event"`
}

func channelCreated(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	req := new(channelCreatedRequest)
	fmt.Println("request.Body", request.Body)
	if err := json.Unmarshal([]byte(request.Body), req); err != nil {
		return nil, err
	}

	if err := post(req.Event.Channel.Name); err != nil {
		return nil, err
	}

	return &events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       "200 OK",
	}, nil
}

func post(message string) error {
	req, err := http.NewRequest(
		"POST",
		"webhook url",
		bytes.NewBuffer([]byte(fmt.Sprintf(`{"text": "チャンネルが追加されました #%s"}`, message))),
	)
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return errors.New(fmt.Sprintf("Send message is failer: %d", resp.StatusCode))
	}

	return nil
}

func main() {
	lambda.Start(channelCreated)
}
